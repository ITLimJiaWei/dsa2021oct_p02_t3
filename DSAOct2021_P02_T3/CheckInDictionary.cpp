#include "CheckInDictionary.h"
using namespace std;

// constructor
CheckInDictionary::CheckInDictionary()
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		items[i] = NULL; //to get rid of rubbish that cpp produces
	}
	size = 0;
};

// destructor is to remove dynamic memory
// static memory is destroyed when program ends, but not dynamic memory
// e.g int i will be destroyed becasue its static but new Node of Node* node = new Node(); will still be there
// but Node* node will be gone/the memory address pointing to it
CheckInDictionary::~CheckInDictionary() {
	for (int i = 0; i < MAX_SIZE; i++)
	{
		if (items[i] != NULL) {
			Node* temp = items[i];
			while (temp != NULL) 
			{
				items[i] = temp->next; //2
				temp->next = NULL; //3
				delete temp; //4

				temp = items[i]; //goes to next node to destroy /5
			}
		}
	}	
};

int charvalue(char c)
{
	if (isalpha(c))
	{
		
		if (isupper(c))
			return (int)c - (int)'A';
		else 
			return (int)c - (int)'a' + 26;
		
			
	}
	else
		return 32;
}


int CheckInDictionary::hash(KeyType key)
{
	int total = charvalue(key[0]);
	for (int i = 1; i < key.size(); i++) {

		total = (total * 52 + charvalue(key[i]))%MAX_SIZE;
		
	}
	return total;
};

// add a new item with the specified key to the Dictionary (Key for LL is checkindate)
// pre : none
// post: new item is added to the Dictionary
//       size of Dictionary is increased by 1
bool CheckInDictionary::add(KeyType newKey, ItemType newItem)
{
	//Compute the index using hash function
	int index = hash(newKey);
	string checkinDate = newItem.getcheckindate();
	const char* c = checkinDate.c_str();
	tm date1;
	sscanf_s(c, "%d/%d/%4d  %d:%d:%d",
	    &date1.tm_mday, &date1.tm_mon, &date1.tm_year, &date1.tm_hour, &date1.tm_min, &date1.tm_sec);
	date1.tm_mon = date1.tm_mon - 1;
	date1.tm_year = date1.tm_year - 1900;
	date1.tm_hour = 0;
	date1.tm_min = 0;
	date1.tm_sec = 0;
	time_t daysSinceEpoch = mktime(&date1); //time since 1970
	
	//If list at index is empty
	if (items[index] == NULL) {

		//Create a new node
		Node* newNode = new Node;
		newNode->item = newItem;
		newNode->key = daysSinceEpoch;
		newNode->next = NULL; // set to NULL because list is empty

		//Set list at index to point to new node
		items[index] = newNode; // adds the node to the list
	}
	else {
		//Set current(pointer) to point to first node at index
		Node* current = items[index];
		//If current node has same key
		if (current->key == daysSinceEpoch) {
			//Return false
			return false;
		}
		//While not at last node
		while (current->next != NULL) //this means it will go to last node
		{
			//Go to next node
			current = current->next;

			if (current->key == daysSinceEpoch) {
				//Return false
				return false;
			}
		}
		//reached the last node

		//Create a new node
		Node* newNode = new Node;
		newNode->item = newItem;
		newNode->key = daysSinceEpoch;
		newNode->next = NULL;
		//Set last node to point to the new node
		current->next = newNode;
	}	

	//Increase the size by 1
	//Return true 
	size++;
	return true;
};

// remove an item with the specified key in the Dictionary
// pre : key must exist in the Dictionary
// post: item is removed from the Dictionary
//       size of Dictionary is decreased by 1
bool CheckInDictionary::remove(KeyType key, string checkinDate)
{
	//Compute the index using hash function
	int index = hash(key);
	const char* c = checkinDate.c_str();
	tm date1;
	sscanf_s(c, "%d/%d/%4d  %d:%d:%d",
		&date1.tm_mday, &date1.tm_mon, &date1.tm_year, &date1.tm_hour, &date1.tm_min, &date1.tm_sec);
	date1.tm_mon = date1.tm_mon - 1;
	date1.tm_year = date1.tm_year - 1900;
	date1.tm_hour = 0;
	date1.tm_min = 0;
	date1.tm_sec = 0;
	time_t secondsFromEpoch = mktime(&date1);
	/*If list at index is not empty
		Perform list remove of item with specified key
		Decrease the size by 1*/
	if (items[index] != NULL) {

		if (items[index]->key == secondsFromEpoch) {
			Node* current = items[index];
			items[index] = items[index]->next;
			current->next = NULL;  // clearing memory space
			delete current;
			return true;
		}
		else {
			Node* current = items[index];

			while (current->next != NULL) //this means it will go to last node
			{
				if (current->next->key == secondsFromEpoch) {
					Node* previousNode = current;
					current = current->next;
					previousNode->next = current->next;
					current->next = NULL;
					delete current;
					break;
					
				}
				current = current->next;
				
				
			}
		}
		return true;
		size--;
	}
	return false;
};


// get an item with the specified key in the Dictionary (retrieve) 
// pre : key must exist in the dictionary
// post: none
// return the item with the specified key from the Dictionary
ItemType CheckInDictionary::get(KeyType key, string checkinDate)
{
	//Compute the index using hash function
	int index = hash(key);

	const char* c = checkinDate.c_str();
	tm date1;
	sscanf_s(c, "%d/%d/%4d  %d:%d:%d",
		&date1.tm_mday, &date1.tm_mon, &date1.tm_year, &date1.tm_hour, &date1.tm_min, &date1.tm_sec);
	date1.tm_year = date1.tm_year - 1900;
	date1.tm_mon = date1.tm_mon - 1;
	date1.tm_hour = 0;
	date1.tm_min = 0;
	date1.tm_sec = 0;


	time_t daysSinceEpoch = mktime(&date1); //time since 1970

	if (items[index] != NULL) {
		if (items[index]->key == daysSinceEpoch) {
			return items[index]->item;
		}
		else {
			Node* current = items[index];

			while (current->next != NULL) //this means it will go to last node
			{
				//Go to next node
				current = current->next; // the node to be retrieved

				if (current->key == daysSinceEpoch) { // the node to be retrieved
					return current->item;
				}
			}
		}

	}
	
	//return empty booking
	Booking booking;
	return booking;
};

// check if the Dictionary is empty
// pre : none
// post: none
// return true if the Dictionary is empty; otherwise returns false
bool CheckInDictionary::isEmpty() {
	if (getLength() > 0) 
	{ 
		return true; 
	} 
	else {
		return false;
	}
};

// check the size of the Dictionary
// pre : none
// post: none
// return the number of items in the Dictionary
int CheckInDictionary::getLength() { return size; };

//------------------- Other useful functions -----------------

// display the items in the Dictionary
void CheckInDictionary::print()
{
	for (int i = 0;i < MAX_SIZE; i++ ) {

		if (items[i] != NULL) {
			

			Node* current = items[i];
			cout << current->key << endl;
			while (current->next != NULL) //this means it will go to 2nd last node
			{
				cout << items[i]->next->key << endl; // printing the current node

				//Go to next node
				current = current->next; 

			}
		}
		
		
		
	}

};

// void replace(KeyType key, ItemType item);
// bool contains(KeyType key);
