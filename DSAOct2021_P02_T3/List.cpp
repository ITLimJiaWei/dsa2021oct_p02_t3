// List.cpp - Implementation of List ADT using Pointers
#include "List.h"  // header file
#include<string>
#include<iostream>
using namespace std;
// constructor
List::List() { }


// add a new item to the back of the list
bool List::add(ItemTypeList newItem) {
	Node* newNode = new Node;
	newNode->item = newItem;
	newNode->next = NULL;

	Node* current = firstNode;

	if (size == 0) {
		firstNode = newNode;
	}
	else {
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = newNode;
	}
	size++;
	return true;
};

// add a new item at a specified position in the list (insert)

bool List::add(int index, ItemTypeList newItem)
{
	if (index >= 0 && index < size ) {
		Node* newNode = new Node;
		newNode->item = newItem;
		newNode->next = NULL;

		if (index==0) {
			newNode->next = firstNode;
			firstNode = newNode;
			size++;
			return true;
		}
		else {
			Node* current = firstNode;
			for (int i = 0; i < index - 1; i++) {
				current = current->next;
			}
			newNode->next = current->next;
			current->next = newNode;

			size ++;
			return true;
		}
	}
	else {
		return false;
	}
};

// remove an item at a specified position in the list
void List::remove(int index) 
{
	if (index >= 0 && index < size) {
		if (index==0) {
			Node* temp = firstNode;
			firstNode = firstNode->next; // firstNode points to 2nd Node;
			temp->next = NULL; // make sure temp does not point to anywhere
			delete temp; //delete the firstNode
		}
		else {
			Node* current = firstNode;
			for (int i = 0; i < index - 1; i++) {
				current = current->next;
			}
			Node* temp = current->next; 
			current->next = current->next->next; // previous Node pointing to Node after the deleted Node
			temp->next = NULL; //making sure the deleted Node's memory is NULL
			delete temp;
		}
		size--;
	}
	
};

// remove an item at a specified position in the list
bool List::removeByRoomNo(string roomNo)
{
	int index; 
	index = getLength(); //get full size of room
	Node* current = firstNode;
	if (current->item.getRoomNo() == roomNo) {
		Node* temp = firstNode;
		current = current->next; // first node now points to 2nd node
		temp->next = NULL; // make sure temp does not point to anywhere
		delete temp;
		size--;
		return false;
	}
	else {
		Node* previous = firstNode;;
		for (int i = 0; i < index - 1; i++) {
			if (current->item.getRoomNo() == roomNo) {
				previous->next = current->next; // previous Node pointing to Node after the deleted Node
				current->next = NULL;  //making sure the deleted Node's memory is NULL
				delete current;
				size--;
				return true;
			}

			previous = current;
			current = current->next;
		}
	}
	size--;
	return false;
};


// get an item at a specified position of the list (retrieve)
ItemTypeList List::get(int index)
{
	if (index >= 0 && index < size) {
		Node* current = firstNode;
		for (int i = 0; i < index; i++) {
			current = current->next;
		}
		return current->item;
	}
	else {
		Room room;
		return room;
	}
};

// check if the list is empty
bool List::isEmpty() 
{
	if (firstNode->next == NULL) {
		return true;
	}
	else {
		return false;
	}
};

// check the size of the list
int List::getLength() 
{
	return size;
};

// display all the items in the list
void List::print() 
{
	Node* current = firstNode;
	while (current != NULL) {
		cout << current->item.getRoomNo();
		if (current->next != NULL) {
			cout << " , ";
		}
		current = current->next;
	}
	cout << endl;
};
