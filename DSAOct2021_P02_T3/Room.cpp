#include "Room.h"


Room::Room() {}

Room::Room(string r, string rt, double cpn)
{
	roomNo = r;
	roomType = rt;
	costPerNight = cpn;
}

void Room::setRoomNo(string r)
{
	roomNo = r;
}
string Room::getRoomNo()
{
	return roomNo;
}
void Room::setRoomType(string rt)
{
	roomType = rt;
}
string Room::getRoomType()
{
	return roomType;
}
void Room::setCostPerNight(double cost)
{
	costPerNight = cost;
};
double Room::getCostPerNight()
{
	return costPerNight;
};
