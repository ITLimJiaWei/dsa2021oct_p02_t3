// Group Name: Team 3
// Team Members: Lim Jia Wei - S10204971J, Simon Christian Lugi Soriano - S10204949B

#pragma once
using namespace std;
#include <string>

class Room
{
	private:
		string roomNo;
		string roomType;
		double costPerNight;
	public:
		Room();
		Room(string,string,double);
		void setRoomNo(string);
		string getRoomNo();
		void setRoomType(string);
		string getRoomType();
		void setCostPerNight(double);
		double getCostPerNight();
};
