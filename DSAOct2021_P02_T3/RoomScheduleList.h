// Group Name: Team 3
// Team Members: Lim Jia Wei - S10204971J, Simon Christian Lugi Soriano - S10204949B
//
// Notable Features
// Being a typical LinkedList, it has is it O(n). However, in most of its implementation, minimal data is stored in it 
// which results in it being quite memory efficient because of its dynamic nature and very efficient for the use case


// List.h - - Specification of List ADT

#include<string>
#include<iostream>
#include"Room.h"
#include"Booking.h"

using namespace std;

typedef Booking RoomScheduleItemType;

class RoomScheduleList
{
private:
	struct Node
	{
		RoomScheduleItemType item;	// data item
		Node* next;	// pointer pointing to next item
	};

	Node* firstNode;	// point to the first item
	int  size;			// number of items in the list

public:

	// constructor
	RoomScheduleList();


	// add a new item to the back of the list (append)
	// pre : none
	// post: new item is added to the back of the list
	//       size of list is increased by 1
	bool add(RoomScheduleItemType newItem);

	// add a new item at a specified position in the list (insert)
	// pre : 0 <= index <= size
	// post: new item is added to the specified position in the list
	//       items after the position are shifted back by 1 position
	//       size of list is increased by 1
	bool add(int index, RoomScheduleItemType newItem);

	// remove an item at a specified position in the list
	// pre : 0 <= index <= size
	// post: item is removed the specified position in the list
	//       items after the position are shifted forward by 1 position
	//       size of list is decreased by 1
	void remove(int index);

	// get an item at a specified position of the list (retrieve)
	// pre : 0 <= index <= size
	// post: none
	// return the item in the specified index of the list
	RoomScheduleItemType get(int index);

	// check if the list is empty
	// pre : none
	// post: none
	// return true if the list is empty; otherwise returns false
	bool isEmpty();

	// check the size of the list
	// pre : none
	// post: none
	// return the number of items in the list
	int getLength();

	//------------------- Other useful functions -----------------

	// display the items in the list
	void print();

	// void replace(int index, ItemType item);
	// int search(ItemType item);
};