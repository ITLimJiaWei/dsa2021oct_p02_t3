#include "Booking.h"
#include <iostream>


Booking::Booking() {};
Booking::Booking(int id, string bd, string gn, string rn, string rt, string s, string cid, string cod, int gNo, string sq)
{
	bookingID = id;
	bookingDate = bd;
	guestName = gn;
	roomNo = rn;
	roomType = rt;
	status = s;
	checkInDate = cid;
	checkOutDate = cod;
	guestNo = gNo;
	specialReq = sq;
};


void Booking::setbookingid(int id) { bookingID = id; };
int Booking::getbookingid() { return bookingID; };

void Booking::setbookingdate(string bd) { bookingDate = bd; };
string Booking::getbookingdate() { return bookingDate; };

void Booking::setguestname(string gName) { guestName = gName; };
string Booking::getguestname() { return guestName; };

void Booking::setroomno(string rn) { roomNo = rn; };
string Booking::getroomno() { return roomNo; };

void Booking::setroomtype(string rt) { roomType = rt; };
string Booking::getroomtype() { return roomType; };

void Booking::setstatus(string s) { status = s; };
string Booking::getstatus() { return status; };

void Booking::setcheckindate(string cid) { checkInDate = cid; };
string Booking::getcheckindate() { return checkInDate; };

void Booking::setcheckoutdate(string cod) { checkOutDate = cod; };
string Booking::getcheckoutdate() { return checkOutDate; };

void Booking::setguestno(int gNo) { guestName = gNo; };
int Booking::getguestno() { return guestNo; };

void Booking::setspecialreq(string sq) { specialReq = sq; };
string Booking::getspecialreq() { return specialReq; };

