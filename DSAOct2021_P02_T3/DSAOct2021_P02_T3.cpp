// DSAOct2021_P02_T3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;
#include <fstream>
#include <sstream>
#include "Booking.h"
#include "Room.h"
#include "List.h" 
#include "CheckInDictionary.h" //FOR Q1 
#include "RoomScheduleList.h" //FOR Q1 AND Q2
#include "DateDictionary.h"
#include "Interval.h"
#include <vector>
#include <chrono>
#include <time.h>
#pragma warning(disable:4996)

using namespace std::chrono;

//#include "AVLDictionary.h"

void displayMenu(string currentDate);

bool compareInterval(Interval i1, Interval i2);

bool isIntersect(Interval arr[], int n);

time_t convertToEpoch(string date);

int getNewBookingID();

void update_bookingFile(Booking booking);

DateDictionary getAllBookingsUsingDD();

int main()
{

    //Initialize ADTS here
    CheckInDictionary CheckInDict;
    RoomScheduleList q3List;

    //Extract rooms
    ifstream inputFile;
    
    string line = "";

    //Open bookings file
    inputFile.open("Bookings.csv"); //Bookings csv

    while (getline(inputFile, line)) {
        //declaring booking attributes
        string bookingIDget;
        string bookingDate;
        string guestName;
        string roomNo;
        string roomType;
        string status;
        string checkInDate;
        string checkOutDate;
        string guestNoget;
        string specialReq;
        string tempString = "";

        stringstream inputString(line);
        getline(inputString, bookingIDget, ',');

        if (bookingIDget != "Booking ID") //skips the first row of headers
        {
            //populating booking variables
            int bookingID = stoi(bookingIDget);
            getline(inputString, bookingDate, ',');
            getline(inputString, guestName, ',');
            getline(inputString, roomNo, ',');
            getline(inputString, roomType, ',');
            getline(inputString, status, ',');
            getline(inputString, checkInDate, ',');
            getline(inputString, checkOutDate, ',');
            getline(inputString, guestNoget, ',');

            int guestNo = stoi(guestNoget);
            getline(inputString, specialReq, ',');

            //Creating new booking object and storing it in checkinDict and q3List
            Booking newBooking(bookingID, bookingDate, guestName, roomNo, roomType, status, checkInDate, checkOutDate, guestNo, specialReq); //fill in the object with the csv
            CheckInDict.add(guestName,newBooking);
            q3List.add(newBooking);
        }  
    }

    inputFile.close();

    //getting current time
    const int MAXLEN = 80;
    char s[MAXLEN];
    time_t t = time(0);
    strftime(s, MAXLEN, "%d/%m/%Y", localtime(&t));

    //converting char array to string
    string currentDate(s);

    // main menu
	int option = -1;

	while (option != 0)
	{
		displayMenu(currentDate);
		cin >> option;

		if (option == 1) {

            //Delcaring ADTS for Task 1
            RoomScheduleList bookingsList;
            List roomsList;
       
            string gName;
            cin.ignore();

            //getting Guest Name
            cout << "Please enter your guest name to check in: ";
            getline(cin,gName);

            //checks if there the guest name input exists in the check-in dictionary
            if (CheckInDict.get(gName, currentDate).getguestname() == "") 
            {
                cout << "Apologies, no booking exists with that name and check-in date. Please try again" << endl;
                //outputs an error message if it does not exist
            }
            else {
                //if the name goes exist, get the booking of the guest
                Booking booking = CheckInDict.get(gName, currentDate);
                //Populate bookings list only if its checked in
                //Open bookings file
                inputFile.open("Bookings.csv"); //Bookings csv
                while (getline(inputFile, line)) {
                    //declaring booking variables
                    string bookingIDget;
                    string bookingDate;
                    string guestName;
                    string roomNo;
                    string roomType;
                    string status;
                    string checkInDate;
                    string checkOutDate;
                    string guestNoget;
                    string specialReq;
                    string tempString = "";

                    stringstream inputString(line);
                    getline(inputString, bookingIDget, ',');

                    if (bookingIDget != "Booking ID") //skips the first row of headers
                    {
                        //populating booking variables
                        int bookingID = stoi(bookingIDget);

                        getline(inputString, bookingDate, ',');
                        getline(inputString, guestName, ',');
                        getline(inputString, roomNo, ',');
                        getline(inputString, roomType, ',');
                        getline(inputString, status, ',');
                        getline(inputString, checkInDate, ',');
                        getline(inputString, checkOutDate, ',');
                        getline(inputString, guestNoget, ',');
                        int guestNo = stoi(guestNoget);
                        getline(inputString, specialReq, ',');

                        //Add booking only if checked in (Filter out status and roomtype to get specific rooms available)
                        if (status == "Checked In" && roomType == booking.getroomtype()) {
                            Booking newBooking(bookingID, bookingDate, guestName, roomNo, roomType, status, checkInDate, checkOutDate, guestNo, specialReq); //fill in the object with the csv
                            bookingsList.add(newBooking);
                            //results in very minimal amount of records in the booking linkedlist 
                        }
                    }
                }
                inputFile.close(); //Bookings csv
                ifstream inputFile;
                //Open room file
                inputFile.open("Rooms.csv"); //Rooms csv
                string line = "";
                while (getline(inputFile, line)) {
                    //declaring room variables
                    string roomNo;
                    string roomType;
                    double costPerNight;
                    string tempString = "";
                    stringstream inputString(line);

                    //populating room variables
                    getline(inputString, roomNo, ',');
                    getline(inputString, roomType, ',');
                    getline(inputString, tempString, ',');

                    costPerNight = atof(tempString.c_str());

                    if (roomNo != "Room #" && roomType == booking.getroomtype()) //skips the first row of headers and filters out room by type
                    {
                        Room newRoom(roomNo, roomType, costPerNight);
                        roomsList.add(newRoom);
                    }
                }
                //closing inputfile
                inputFile.close();
                string userCheckInDate = booking.getcheckindate();
                string userCheckOutDate = booking.getcheckoutdate();

                time_t userCheckIn = convertToEpoch(userCheckInDate);
                time_t userCheckOut = convertToEpoch(userCheckOutDate);

                //Now, compare the current checkin to the bookings to see which room are available
                //Check if checkin/checkout dates of bookings match with man
                for (int i = 0; i < bookingsList.getLength(); i++) {
;
                    //Create a temporary booking to store checked booking
                    Booking checkBooking = bookingsList.get(i);
                    string check1 = checkBooking.getcheckindate();
                    string check2 = checkBooking.getcheckoutdate();
                    //Create intervals
                  
                    time_t compareCheckIn = convertToEpoch(check1);
                    time_t compareCheckOut = convertToEpoch(check2);

                    Interval compareIntervals[] = { {userCheckIn, userCheckOut}, {compareCheckIn, compareCheckOut} };

                    int comparison = sizeof(compareIntervals) / sizeof(compareIntervals[0]);
                    //Check if dates clash, if clash, remove room from roomlist
                    if (isIntersect(compareIntervals, comparison)) {
                       //Delete the room that has the intersection
                        string roomNo = checkBooking.getroomno();
                        roomsList.removeByRoomNo(roomNo);
                    }
                }
                cout << "Room Number " << " Cost/Night" << endl;
                cout << "---------------------------" << endl;
                for (int i = 0; i < roomsList.getLength(); i++) {
                    Room room = roomsList.get(i);
                    cout << i + 1 << ".) " << room.getRoomNo() << "    " << room.getCostPerNight() << endl;
                }
                string roomIndex;
                int input;
                //check if user selected a proper room
                bool check = true;
                while (check) {
                    cout << "Please select a room: ";
                    getline(cin, roomIndex);
                    input = stoi(roomIndex);
                    if (roomsList.get(input - 1).getRoomNo() != "") {
                        check = false;
                        booking.setstatus("Checked In");
                        booking.setroomno(roomsList.get(input - 1).getRoomNo());
                        cout << "You have been successfully checked in to " << booking.getroomno() <<  ". We hope you enjoy your stay!";

                        
                    }
                    else {
                        cout << "Invalid option! Please try again." << endl;
                    }
                }
            }

		}
		else if (option == 2)
		{
            //declaring ADTs and variables for user input
            RoomScheduleList bookingsList;
            List roomsList;
            cin.ignore();
            string name;
            string checkindate;
            string checkoutdate;

            //populating user inputs
            cout << "Please enter your booking details one at a time." << endl;
            cout << "-----------------------" << endl;
            cout << "Please enter your name: ";
            getline(cin, name);

            cout << "Please enter your check-in date(DD/MM/YYYY): ";
            getline(cin, checkindate);

            cout << "Please enter your checkout date(DD/MM/YYYY): ";
            getline(cin, checkoutdate);
            
            inputFile.open("Bookings.csv"); //Bookings csv
            while (getline(inputFile, line)) {
                //declaring booking variables
                string bookingIDget;
                string bookingDate;
                string guestName;
                string roomNo;
                string roomType;
                string status;
                string checkInDate;
                string checkOutDate;
                string guestNoget;
                string specialReq;
                string tempString = "";

                stringstream inputString(line);
                getline(inputString, bookingIDget, ',');

                if (bookingIDget != "Booking ID") //skips the first row of headers
                {
                    //populating booking variables
                    int bookingID = stoi(bookingIDget);
                    getline(inputString, bookingDate, ',');
                    getline(inputString, guestName, ',');
                    getline(inputString, roomNo, ',');
                    getline(inputString, roomType, ',');
                    getline(inputString, status, ',');
                    getline(inputString, checkInDate, ',');
                    getline(inputString, checkOutDate, ',');
                    getline(inputString, guestNoget, ',');
                    int guestNo = stoi(guestNoget);
                    getline(inputString, specialReq, ',');

                    //Add booking only if checked in (Filter out status and roomtype to get specific rooms available)
                    if (status == "Checked In" || status == "Booked") {
                        Booking newBooking(bookingID, bookingDate, guestName, roomNo, roomType, status, checkInDate, checkOutDate, guestNo, specialReq); //fill in the object with the csv
                        bookingsList.add(newBooking);
                    }
                }
            }
            inputFile.close();
            ifstream inputFile;
            //Open room file

            inputFile.open("Rooms.csv"); //Rooms csv
            string line = "";
            while (getline(inputFile, line)) {
                //declaring room variables
                string roomNo;
                string roomType;
                double costPerNight;
                string tempString = "";
                stringstream inputString(line);
                //populating room variables
                getline(inputString, roomNo, ',');
                getline(inputString, roomType, ',');
                getline(inputString, tempString, ',');
                costPerNight = atof(tempString.c_str());

                if (roomNo != "Room #") //skips the first row of headers 
                {
                    //creating and adding a new room object to the roomsList
                    Room newRoom(roomNo, roomType, costPerNight);
                    roomsList.add(newRoom);
                }
            }

            //converting to seconds from epoch
            time_t userCheckIn = convertToEpoch(checkindate);
            time_t userCheckOut = convertToEpoch(checkoutdate);

            //Now, compare the current checkin to the bookings to see which room are available
            //Check if checkin/checkout dates of bookings match with man
            for (int i = 0; i < bookingsList.getLength(); i++) {
                
                //Create a temporary booking to store checked booking
                Booking checkBooking = bookingsList.get(i);
                string check1 = checkBooking.getcheckindate();
                string check2 = checkBooking.getcheckoutdate();

                time_t compareCheckIn = convertToEpoch(check1);
                time_t compareCheckOut = convertToEpoch(check2);

                //Create intervals
                Interval compareIntervals[] = { {userCheckIn, userCheckOut}, {compareCheckIn, compareCheckOut} };

                int comparison = sizeof(compareIntervals) / sizeof(compareIntervals[0]);
                //Check if dates clash, if clash, remove room from roomlist
                if (isIntersect(compareIntervals, comparison)) {
                    //Delete the room that has the intersection
                    string roomNo = checkBooking.getroomno();
                    roomsList.removeByRoomNo(roomNo);
                }
            }

            //showcase available rooms to the user
            cout << "--------Available rooms-------- " << endl;

            cout << "Room Number " << " Room Type " << "     Cost/Night" << endl;
            for (int i = 0; i < roomsList.getLength(); i++) {
                Room room = roomsList.get(i);
                cout << i + 1 << ".) " << room.getRoomNo() << " " << room.getRoomType() <<  "     " << room.getCostPerNight() << endl;
            }

            string roomIndex;
            int input;
            //check if user selected a proper room
            bool check = true;
            while (check) {
                cout << "Please select a room: ";
                getline(cin, roomIndex);
                input = stoi(roomIndex);
                if (roomsList.get(input - 1).getRoomNo() != "") {
                    string guestNoGet;
                    string specialreq;
                    check = false;
                    cout << "Please input the number of guests: ";
                    getline(cin, guestNoGet);
                    int guestNo = stoi(guestNoGet);
                    cout << "Please input any special requests: ";
                    getline(cin, specialreq);
                    
                    int id = getNewBookingID();
                    string status = "Booked";
                    //make new booking object here wtih all the inputs gathered
                    Booking newBooking(id -1,currentDate,name, roomsList.get(input - 1).getRoomNo(), roomsList.get(input - 1).getRoomType(), status, checkindate, checkoutdate, guestNo, specialreq);
                    //updates booking.csv 
                    update_bookingFile(newBooking);
                    cout << "You have succesfully created a booking with BookHoliStay. We hope to see you soon on " << checkindate << "!" << endl;
                }
                else {
                    cout << "Invalid option! Please try again." << endl;
                }
            }
        
		}
		else if (option == 3)
		{
            cin.ignore();
            //getting user input
            string date;
            cout << "Please enter a new date(DD/MM/YYYY): ";
            getline(cin, date);
            time_t convertedDate = convertToEpoch(date);
            cout << "-----------------------------------------" << endl;

            for (int i = 0; i < q3List.getLength(); i++) {
                
                //Create a bookings list to store bookings
                Booking checkBooking = q3List.get(i);
                string check1 = checkBooking.getcheckindate();
                string check2 = checkBooking.getcheckoutdate();

                //Create intervals
                time_t compareCheckIn = convertToEpoch(check1);
                time_t compareCheckOut = convertToEpoch(check2);
                //If given date is within a booking's check in and checkout
                if (compareCheckIn <= convertedDate && compareCheckOut >= convertedDate) {
                    // show the guest names that are staying in the hotel on that particular date
                    cout << checkBooking.getguestname() << endl;
                }
            }
		}
		else if (option == 4)
		{
            cin.ignore();
            string month;
            string year;

            cout << "Please enter a Month (1-12): ";
            getline(cin, month);
            cout << "Please enter a Year (Above 2019): ";
            getline(cin, year);

            string date = "1/" + month + "/" + year;
            //Initialize datedict
            DateDictionary dateDict;
            dateDict = getAllBookingsUsingDD();
            
            ////Print out all bookings in the month
            dateDict.getRoomsbyMonth(date);
		}
        else if (option == 5)
        {
        //intiailize ADTs and user inputs
        RoomScheduleList bookingsList;
        List roomsList;
        cin.ignore();
        string name;
        string checkindate;
        string checkoutdate;

        //getting user inputs
        cout << "Please enter your booking details one at a time." << endl;
        cout << "Please enter your name: ";
        getline(cin, name);

        cout << "Please enter your check-in date(DD/MM/YYYY): ";
        getline(cin, checkindate);

        cout << "Please enter your checkout date(DD/MM/YYYY): ";
        getline(cin, checkoutdate);

        inputFile.open("Bookings.csv"); //Bookings csv
        while (getline(inputFile, line)) {
            //declaring booking variables
            string bookingIDget;
            string bookingDate;
            string guestName;
            string roomNo;
            string roomType;
            string status;
            string checkInDate;
            string checkOutDate;
            string guestNoget;
            string specialReq;
            string tempString = "";

            stringstream inputString(line);
            getline(inputString, bookingIDget, ',');

            if (bookingIDget != "Booking ID") //skips the first row of headers
            {
                //populating booking variables
                int bookingID = stoi(bookingIDget);
                getline(inputString, bookingDate, ',');
                getline(inputString, guestName, ',');
                getline(inputString, roomNo, ',');
                getline(inputString, roomType, ',');
                getline(inputString, status, ',');
                getline(inputString, checkInDate, ',');
                getline(inputString, checkOutDate, ',');
                getline(inputString, guestNoget, ',');
                int guestNo = stoi(guestNoget);
                getline(inputString, specialReq, ',');

                //Add booking only if checked in (Filter out status and roomtype to get specific rooms available)
                if (status == "Checked In" || status == "Booked") {
                    //create and add a new booking to bookingsList
                    Booking newBooking(bookingID, bookingDate, guestName, roomNo, roomType, status, checkInDate, checkOutDate, guestNo, specialReq); //fill in the object with the csv
                    bookingsList.add(newBooking);
                }
            }
        }
        //closes inputFile
        inputFile.close();
        ifstream inputFile;
        //Open room file

        inputFile.open("Rooms.csv"); //Rooms csv
        string line = "";
        while (getline(inputFile, line)) {
            //declaring room variables
            string roomNo;
            string roomType;
            double costPerNight;
            string tempString = "";

            stringstream inputString(line);

            //populating booking variables
            getline(inputString, roomNo, ',');
            getline(inputString, roomType, ',');
            getline(inputString, tempString, ',');

            costPerNight = atof(tempString.c_str());

            if (roomNo != "Room #") //skips the first row of headers 
            {
                Room newRoom(roomNo, roomType, costPerNight);
                roomsList.add(newRoom);
            }
        }
        //closing inputFile
        inputFile.close();

        //converts to seconds from epoch
        time_t userCheckIn = convertToEpoch(checkindate);
        time_t userCheckOut = convertToEpoch(checkoutdate);

        //Now, compare the current checkin to the bookings to see which room are available
        //Check if checkin/checkout dates of bookings match with man
        for (int i = 0; i < bookingsList.getLength(); i++) {

            //Create a temporary booking to store checked booking
            Booking checkBooking = bookingsList.get(i);
            string check1 = checkBooking.getcheckindate();
            string check2 = checkBooking.getcheckoutdate();
            time_t compareCheckIn = convertToEpoch(check1);
            time_t compareCheckOut = convertToEpoch(check2);

            //Create intervals
            Interval compareIntervals[] = { {userCheckIn, userCheckOut}, {compareCheckIn, compareCheckOut} };

            int comparison = sizeof(compareIntervals) / sizeof(compareIntervals[0]);
            //Check if dates clash, if clash, remove room from roomlist
            if (isIntersect(compareIntervals, comparison)) {
                //Delete the room that has the intersection
                string roomNo = checkBooking.getroomno();
                roomsList.removeByRoomNo(roomNo);
            }
        }
        //showcase available rooms
        cout << "--------Available rooms-------- " << endl;

        cout << "Room Number " << " Room Type" << endl;
        for (int i = 0; i < roomsList.getLength(); i++) {
            Room room = roomsList.get(i);
            cout << i + 1 << ".) " << room.getRoomNo() << " " << room.getRoomType() << endl;
        }
        string roomIndex;
        int input;
        //check if user selected a proper room
        bool check = true;
        while (check) {
            cout << "Please select a room: ";
            getline(cin, roomIndex);
            input = stoi(roomIndex);
            if (roomsList.get(input - 1).getRoomNo() != "") {
                string guestNoGet;
                string specialreq;
                check = false;
                cout << "Please input the number of guests: ";
                getline(cin, guestNoGet);
                int guestNo = stoi(guestNoGet);
                cout << "Please input any special requests if any: ";
                getline(cin, specialreq);

                //gets the latest bookingID
                int id = getNewBookingID();
                // checking in the user at the same time as creation of booking
                string status = "Checked In";
                //create a new booking object here wtih all the inputs gathered
                Booking newBooking(id - 1, currentDate, name, roomsList.get(input - 1).getRoomNo(), roomsList.get(input - 1).getRoomType(), status, checkindate, checkoutdate, guestNo, specialreq);
                //rmb to add in code to write to csv here
                update_bookingFile(newBooking);
                cout << "You have been checked in without a booking. We hope you enjoy your stay at BookHoliStay!" << endl;
            }
            else {
                cout << "Invalid option! Please try again." << endl;
            }
        }

        }
        else if (option == 6)
        {
            //get user input
            cin.ignore();
            string name;
            cout << "To delete a booking, please enter guest name: ";
            getline(cin, name);

            string checkInDate;
            cout << "Please input the booking's check-in date: ";
            getline(cin, checkInDate);
            
            //check if deleting was successful
            bool check;
            check = CheckInDict.remove(name, checkInDate);
            if (check) {
                //displaying success message if deletion was successful
                cout << "The booking of guest, " << name << " with a check-in date of " << checkInDate << " has been deleted.";
            } 
            else {
                //displaying error missage if deletion was not successful if guest name or check-in date were wrong
                cout << "The deletion of guest did not succeed. Did you enter a wrong guest name or check-in date?" << endl;
            }
            

        }
        else if (option == 7)
        {
            //function to change "currnet" date
            
            //getting user input
            cin.ignore();
            string newDate;
            cout << "Please enter a new date: ";
            getline(cin, newDate);
            currentDate = newDate;
        }
	}
	return 0;
}

//function to displayMenu
void displayMenu(string currentDate)
{
	cout << endl;
    cout << "The current date is " << currentDate << endl;
	cout << "---------------BookHoliStay---------------\n";
	cout << "1.) Check in using booking information        \n";
	cout << "2.) Add and save a new booking                \n";
	cout << "3.) Display guest staying on a particular date\n";
	cout << "4.) Display for a particular month, the dates that each room is occupied\n";
	cout << "5.) Check in without a booking \n";
	cout << "6.) Delete a booking \n";
    cout << "7.) Change current date(for demo purposes, in format of DD/MM/YYYY) \n";
	cout << "0.) Exit							 \n";
	cout << "-----------------------------------------\n";
	cout << "Enter option: ";
}


// Compares two intervals according to their starting time.
// This is needed for sorting the intervals using library
// function std::sort(). See http:// goo.gl/iGspV
bool compareInterval(Interval i1, Interval i2)
{
    return (i1.start < i2.start) ? true : false;
}

// Function to check if any two intervals overlap
bool isIntersect(Interval arr[], int n)
{
    // Sort intervals in increasing order of start time
    sort(arr, arr + n, compareInterval);

    // In the sorted array, if start time of an interval
    // is less than end of previous interval, then there
    // is an overlap
    for (int i = 1; i < n; i++)
        if (arr[i - 1].end > arr[i].start)
            return true;

    // If we reach here, then no overlap
    return false;
}

//function to convert a string date to secondsfromEpoch in time_t
time_t convertToEpoch(string date) {
    const char* c = date.c_str();
    tm date1;
    sscanf_s(c, "%d/%d/%4d  %d:%d:%d",
        &date1.tm_mday, &date1.tm_mon, &date1.tm_year, &date1.tm_hour, &date1.tm_min, &date1.tm_sec);
    date1.tm_mon = date1.tm_mon - 1;
    date1.tm_year = date1.tm_year - 1900;
    date1.tm_hour = 0;
    date1.tm_min = 0;
    date1.tm_sec = 0;
    time_t secondsSinceEpoch = mktime(&date1);
    return secondsSinceEpoch;
}

//function that gets the latest bookingID by checking with bookings.csv
int getNewBookingID() {
    int id = 1;

    ifstream file;
    //Open booking file
    file.open("Bookings.csv"); 
    string currentline;
    while (getline(file, currentline))
    {
        id += 1;
    }

    return id;
    file.close();
}

//function that updates bookings.csv given a new booking object
void update_bookingFile(Booking booking)
{

    // File pointer
    fstream fin, fout;

    // Open an existing record
    fin.open("Bookings.csv", ios::in);

    // Create a new file to store updated data
    fout.open("BookingsNew.csv", ios::out);

    int count = 0, i;
    string line, word;
    vector<string> row;

    int check = 1;
    // Traverse the file
    while (!fin.eof()) {

        row.clear();

        getline(fin, line);
        stringstream s(line);

        while (getline(s, word, ',')) {
            row.push_back(word);
        }

        int row_size = row.size();
        if (!fin.eof()) {
            for (i = 0; i < row_size - 1; i++) {
                fout << row[i] << ",";
            }
            check += 1;
            fout << row[row_size - 1] << "\n";
        }
        if (fin.eof()) {
            break;
        }
    }
    if (count == 0) {
        //Insert new record
        //Booking newBooking(bookingID, bookingDate, guestName, roomNo, roomType, status, checkInDate, checkOutDate, guestNo, specialReq);
        string bookingId = to_string(booking.getbookingid());
        string guestNo = to_string(booking.getguestno());
        fout << bookingId << "," << booking.getbookingdate() << "," << booking.getguestname() << "," << booking.getroomno() << "," << booking.getroomtype() << ",";
        fout << booking.getstatus() << "," << booking.getcheckindate() << "," << booking.getcheckoutdate() << "," << guestNo << ',' << booking.getspecialreq();
        fout << "\n";

        fin.close();
        fout.close();
    }
    //removes old csv file
    remove("Bookings.csv");
    //renames new file to old csv file name
    rename("BookingsNew.csv", "Bookings.csv");
}

//function for Q4 to get all bookings using datedictionary
DateDictionary getAllBookingsUsingDD() {
    //initialize bookingslist
    DateDictionary bookingsListByDate;
    //Open bookings file
    ifstream inputFile;
    inputFile.open("Bookings.csv"); //Bookings csv
    string line = "";
    while (getline(inputFile, line)) {
        string bookingIDget;
        string bookingDate;
        string guestName;
        string roomNo;
        string roomType;
        string status;
        string checkInDate;
        string checkOutDate;
        string guestNoget;
        string specialReq;
        string tempString = "";

        stringstream inputString(line);
        getline(inputString, bookingIDget, ',');

        if (bookingIDget != "Booking ID") //skips the first row of headers
        {

            int bookingID = stoi(bookingIDget);


            getline(inputString, bookingDate, ',');
            getline(inputString, guestName, ',');
            getline(inputString, roomNo, ',');

            getline(inputString, roomType, ',');
            getline(inputString, status, ',');
            getline(inputString, checkInDate, ',');
            getline(inputString, checkOutDate, ',');

            getline(inputString, guestNoget, ',');

            int guestNo = stoi(guestNoget);
            getline(inputString, specialReq, ',');

            //Room newRoom(roomNo, roomType, costPerNight);
            Booking newBooking(bookingID, bookingDate, guestName, roomNo, roomType, status, checkInDate, checkOutDate, guestNo, specialReq);//fill in the object with the csv
            bookingsListByDate.add(checkInDate, newBooking);
        }
    }

    inputFile.close();
    return bookingsListByDate;
}