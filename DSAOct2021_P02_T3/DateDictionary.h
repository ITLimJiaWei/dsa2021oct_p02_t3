// Group Name: Team 3
// Team Members: Lim Jia Wei - S10204971J, Simon Christian Lugi Soriano - S10204949B
//
// Notable Features
// Check-In date is used as search key and is hashed by comparing the amount of months since January 2019. 
// Booking ID is used as the key for linkedlist in the event where people share the same check-in date as another person
// 
// MAX_SIZE is specified at 199 which is a prime number to reduce the number of collisions as compared to
// a non-prime number. Furthermore, months from 2019 is used as search key to ensure that checking in or deleting a book 
// is efficient as guest name is asked for those 2 tasks. 


#pragma once
#include<string>
#include<iostream>
#include"Booking.h"
#include <stdlib.h>
using namespace std;

const int DateDict_MAX_SIZE = 199;
typedef string KeyTypeDate; //hashing date and month
typedef int KeyTypeID; //bookingID as key 
typedef Booking BookingItem; // Item type


class DateDictionary
{
private:
	struct DateNode
	{
		KeyTypeID key;   // search key for LinkedList chain
		BookingItem item;	// data item
		DateNode* next;	// pointer pointing to next item with same search key
	};

	DateNode* items[DateDict_MAX_SIZE];
	int  size;			// number of items in the Dictionary

public:

	// constructor
	DateDictionary();

	// destructor
	//~DateDictionary();

	// get the month of a string date value
	// pre : date in string form
	// post: month of the date is returned in integer form
	int getMonth(string s);

	// get the year of a string date value
	// pre : date in string form
	// post: year of the date is returned in integer form
	int getYear(string s);

	// hash the date value and returns index of dictionary
	// pre : date in string form
	// post: hashed date value is returned in integer form
	int hashDate(KeyTypeDate datekey);

	// add a booking into the database
	// pre : date of booking, booking item itself
	// post: booking is added to dictionary, size increased by 1
	bool add(KeyTypeDate datekey, BookingItem booking);

	// Get all the rooms that are occupied by month
	// pre : targeted date in string 
	// post: prints all rooms that have been used in that month
	void getRoomsbyMonth(string date);

	//Check if a checkoutdate is within the month
	//pre: check-in date and check-out-date needed
	//post: returns true if check-outdate is within the month, returns false if not
	bool checkSpill(string checkinDate, string checkoutDate);

	// get an item with the specified key in the Dictionary (retrieve)
	// pre : key must exist in the dictionary
	// post: none
	// return the item with the specified key from the Dictionary
	BookingItem get(KeyTypeDate key, int bookingid);
};