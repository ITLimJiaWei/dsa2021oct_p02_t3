// Group Name: Team 3
// Team Members: Lim Jia Wei - S10204971J, Simon Christian Lugi Soriano - S10204949B
//	
// Notable Features
// Guest name is used as search key and is hashed using ASCII. 
// Check-In date is used as the key for linkedlist in the event where people with the same guest name wish 
// to create a booking.
// 
// MAX_SIZE is specified at 199 which is a prime number to reduce the number of collisions as compared to
// a non-prime number. Furthermore, guest name is used as search key to ensure that checking in or deleting a book 
// is efficient as guest name is asked for those 2 tasks. 
// 




#pragma once
#include<string>
#include<iostream>
#include"Booking.h"

using namespace std;

const int MAX_SIZE = 199;	
typedef string KeyType; //hashing guestname
typedef int KeyTypeLL; //checkinDate as key for LinkedList chain
typedef Booking ItemType;



class CheckInDictionary
{
private:
	struct Node
	{
		KeyTypeLL key;   // search key for LinkedList chain
		ItemType item;	// data item
		Node     *next;	// pointer pointing to next item with same search key
	};

	Node *items[MAX_SIZE];
	int  size;			// number of items in the Dictionary

public:

	// constructor
	CheckInDictionary();

	// destructor
	~CheckInDictionary();

	int hash(KeyType key);

	// add a new item with the specified key to the Dictionary
	// pre : none
	// post: new item is added to the Dictionary
	//       size of Dictionary is increased by 1
	bool add(KeyType newKey, ItemType newItem);

	// remove an item with the specified key in the Dictionary
	// pre : key must exist in the Dictionary
	// post: item is removed from the Dictionary
	//       size of Dictionary is decreased by 1
	bool remove(KeyType key, string checkinDate);


	// get an item with the specified key in the Dictionary (retrieve)
	// pre : key must exist in the dictionary
	// post: none
	// return the item with the specified key from the Dictionary
	ItemType get(KeyType key, string checkinDate);

	// check if the Dictionary is empty
	// pre : none
	// post: none
	// return true if the Dictionary is empty; otherwise returns false
	bool isEmpty();

	// check the size of the Dictionary
	// pre : none
	// post: none
	// return the number of items in the Dictionary
	int getLength();

	//------------------- Other useful functions -----------------

	// display the items in the Dictionary
	void print();

	// void replace(KeyType key, ItemType item);
	// bool contains(KeyType key);
};
