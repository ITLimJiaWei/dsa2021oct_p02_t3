#include "DateDictionary.h"
// constructor
DateDictionary::DateDictionary()
{
	for (int i = 0; i < DateDict_MAX_SIZE; i++)
	{
		items[i] = NULL; //to get rid of rubbish that cpp produces
	}
	size = 0;
};

// destructor is to remove dynamic memory
// static memory is destroyed when program ends, but not dynamic memory
// e.g int i will be destroyed becasue its static but new Node of Node* node = new Node(); will still be there
// but Node* node will be gone/the memory address pointing to it
//DateDictionary::~DateDictionary() {
//	for (int i = 0; i < DateDict_MAX_SIZE; i++)
//	{
//		if (items[i] != NULL) {
//			DateNode* temp = items[i];
//			while (temp != NULL)
//			{
//				items[i] = temp->next; //2
//				temp->next = NULL; //3
//				delete temp; //4
//
//				temp = items[i]; //goes to next node to destroy /5
//			}
//		}
//	}
//};

int DateDictionary::getMonth(string s) {
	string delimiter = "/";
	size_t pos = 0;
	string get;
	string month;
	int count = 0;
	while ((pos = s.find(delimiter)) != string::npos) {
		count += 1;
		get = s.substr(0, pos);
		if (count == 2) {
			month = get;
		}
		s.erase(0, pos + delimiter.length());
	}

	int convert = stoi(month);
	return convert;
}

int DateDictionary::getYear(string s) {
	string delimiter = "/";
	size_t pos = 0;
	string year;
	int count = 0;
	while ((pos = s.find(delimiter)) != string::npos) {
		s.substr(0, pos);
		s.erase(0, pos + delimiter.length());
		if (count == 1) {
			year = s;
		}
		count += 1;
	}

	int convert = stoi(year);

	return convert;
}

int DateDictionary::hashDate(KeyTypeDate datekey)
{
	int m = getMonth(datekey);
	int y = getYear(datekey);

	//Get year diff from 2019
	int year = 2019;
	int yDiff = y - 2019;


	//Math to find hashing
	int hashedVal = 12 * yDiff + m;
	//combine both values to get the index value

	return hashedVal;

};

// add a new item with the specified key to the Dictionary (Key for LL is checkindate)
// pre : none
// post: new item is added to the Dictionary
//       size of Dictionary is increased by 1
bool DateDictionary::add(KeyTypeDate newKey, BookingItem newItem)
{
	//Compute the index using hash function
	int index = hashDate(newKey);
	int bookingkey = newItem.getbookingid();
	//If list at index is empty
	if (items[index] == NULL) {

		//Create a new node
		DateNode* newNode = new DateNode;
		newNode->item = newItem;
		newNode->key = bookingkey;
		newNode->next = NULL; // set to NULL because list is empty

		//Set list at index to point to new node
		items[index] = newNode; // adds the node to the list
	}
	else {
		//Set current(pointer) to point to first node at index
		DateNode* current = items[index];
		//If current node has same key
		if (current->key == bookingkey) {
			//Return false
			return false;
		}
		//While not at last node
		while (current->next != NULL) //this means it will go to last node
		{
			//Go to next node
			current = current->next;

			if (current->key == bookingkey) {
				//Return false
				return false;
			}
		}
		//reached the last node

		//Create a new node
		DateNode* newNode = new DateNode;
		newNode->item = newItem;
		newNode->key = bookingkey;
		newNode->next = NULL;
		//Set last node to point to the new node
		current->next = newNode;
	}

	//Increase the size by 1
	//Return true 
	size++;
	return true;
};


bool DateDictionary::checkSpill(string checkoutdate, string date) {

	//check if checkout date is same month as string date
	int coMonth = getMonth(checkoutdate);
	int inputMonth = getMonth(date);
	if (coMonth == inputMonth) {
		return true;
	}
	return false;
}

// get an item with the specified key in the Dictionary (retrieve) 
// pre : key must exist in the dictionary
// post: none
// return the item with the specified key from the Dictionary
BookingItem DateDictionary::get(KeyTypeDate key, int bookingid)
{
	//Compute the index using hash function
	int index = hashDate(key);

	if (items[index] != NULL) {
		if (items[index]->key == bookingid) {
			return items[index]->item;
		}
		else {
			DateNode* current = items[index];

			while (current->next != NULL) //this means it will go to last node
			{
				//Go to next node
				current = current->next; // the node to be retrieved

				if (current->key == bookingid) { // the node to be retrieved
					return current->item;
				}
			}
		}

	}

	//return empty booking
	Booking booking;
	return booking;
};

//------------------- Other useful functions -----------------

// display the items in the Dictionary
void DateDictionary::getRoomsbyMonth(string date)
{
	//get all bookings for the month by checkinid
	int month = hashDate(date);
	int spillMonths = month - 1;
	string check;
	if (items[month] != NULL) {
		cout << "------------------------------------------------------------------------------------------" << endl;
		cout << "Rooms that have been occupied under selected month" << endl;
		cout << "------------------------------------------------------------------------------------------" << endl;
		DateNode* current = items[month];
		while (current->next != NULL)
		{
			check = current->item.getroomno();
			if (check != "" && check != " ") {
				cout << current->item.getroomno() << ": Check-in Date: " << current->item.getcheckindate() << ": Checkout Date: " << current->item.getcheckoutdate() << endl;
			}
			current = current->next;
		}
		if (current->next == NULL) {
			check = current->item.getroomno();
			if (check != "" && check != " ") {
				cout << current->item.getroomno() << ": Check-in Date: " << current->item.getcheckindate() << ": Checkout Date: " << current->item.getcheckoutdate() << endl;
			}
		}
	}
	if (items[spillMonths] != NULL) {

		DateNode* current2 = items[spillMonths];
		while (current2->next != NULL)
		{
			check = current2->item.getroomno();
			if (checkSpill(current2->item.getcheckoutdate(), date)) {
				if (check != "" && check != " ") {
					cout << current2->item.getroomno() << ": Check-in Date: " << current2->item.getcheckindate() << ": Checkout Date: " << current2->item.getcheckoutdate() << endl;
				}
			}
			current2 = current2->next;
		}
		if (current2->next == NULL) {
			check = current2->item.getroomno();
			if (checkSpill(current2->item.getcheckoutdate(), date)) {
				if (check != "" && check != " ") {
					cout << current2->item.getroomno() << ": Check-in Date: " << current2->item.getcheckindate() << ": Checkout Date: " << current2->item.getcheckoutdate() << endl;
				}
			}
			
		}
	}
};


