// List.cpp - Implementation of List ADT using Pointers
#include "RoomScheduleList.h"  // header file
#include<string>
#include<iostream>
using namespace std;
// constructor
RoomScheduleList::RoomScheduleList() { }


// add a new item to the back of the list
bool RoomScheduleList::add(RoomScheduleItemType newItem) {
	Node* newNode = new Node;
	newNode->item = newItem;
	newNode->next = NULL;

	Node* current = firstNode;

	if (size == 0) {
		firstNode = newNode;
	}
	else {
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = newNode;
	}
	size++;
	return true;
};

// add a new item at a specified position in the list (insert)

bool RoomScheduleList::add(int index, RoomScheduleItemType newItem)
{
	if (index >= 0 && index < size ) {
		Node* newNode = new Node;
		newNode->item = newItem;
		newNode->next = NULL;

		if (index==0) {
			newNode->next = firstNode;
			firstNode = newNode;
			size++;
			return true;
		}
		else {
			Node* current = firstNode;
			for (int i = 0; i < index - 1; i++) {
				current = current->next;
			}
			newNode->next = current->next;
			current->next = newNode;

			size ++;
			return true;
		}
	}
	else {
		return false;
	}
};

// remove an item at a specified position in the list
void RoomScheduleList::remove(int index)
{
	if (index >= 0 && index < size) {
		if (index==0) {
			Node* temp = firstNode;
			firstNode = firstNode->next; // firstNode points to 2nd Node;
			temp->next = NULL; // make sure temp does not point to anywhere
			delete temp; //delete the firstNode
		}
		else {
			Node* current = firstNode;
			for (int i = 0; i < index - 1; i++) {
				current = current->next;
			}
			Node* temp = current->next; 
			current->next = current->next->next; // previous Node pointing to Node after the deleted Node
			temp->next = NULL; //making sure the deleted Node's memory is NULL
			delete temp;
		}
		size--;
	}
	
};

// get an item at a specified position of the list (retrieve)
RoomScheduleItemType RoomScheduleList::get(int index)
{
	if (index >= 0 && index < size) {
		Node* current = firstNode;
		for (int i = 0; i < index; i++) {
			current = current->next;
		}
		return current->item;
	}
};

// check if the list is empty
bool RoomScheduleList::isEmpty()
{
	if (firstNode->next == NULL) {
		return true;
	}
	else {
		return false;
	}
};

// check the size of the list
int RoomScheduleList::getLength()
{
	return size;
};

// display all the items in the list
void RoomScheduleList::print()
{
	Node* current = firstNode;
	while (current != NULL) {
		cout << current->item.getbookingid();
		if (current->next != NULL) {
			cout << " , ";
		}
		current = current->next;
	}
	cout << endl;
};
