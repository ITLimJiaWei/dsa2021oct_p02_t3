#pragma once
// A C++ program to check if any two intervals overlap
#include <algorithm>
#include <iostream>
using namespace std;

// An interval has start time and end time
struct Interval {
    int start;
    int end;
};
