// Group Name: Team 3
// Team Members: Lim Jia Wei - S10204971J, Simon Christian Lugi Soriano - S10204949B


#pragma once
using namespace std;
#include <string>

class Booking
{
	private:
		
		int bookingID;
		string bookingDate;
		string guestName;
		string roomNo;
		string roomType;
		string status;
		string checkInDate;
		string checkOutDate;
		int guestNo;
		string specialReq;
		
	public:
		Booking();
		Booking(int, string, string, string, string, string, string, string, int, string);

		/*int hash(KeyType key);*/

		void setbookingid(int);
		int getbookingid();

		void setbookingdate(string);
		string getbookingdate();

		void setguestname(string);
		string getguestname();

		void setroomno(string);
		string getroomno();

		void setroomtype(string);
		string getroomtype();

		void setstatus(string);
		string getstatus();

		void setcheckindate(string);
		string getcheckindate();

		void setcheckoutdate(string);
		string getcheckoutdate();

		void setguestno(int);
		int getguestno();

		void setspecialreq(string);
		string getspecialreq();
};

